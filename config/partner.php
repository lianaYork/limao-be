<?php

return [

    // these options are related to the sign-up procedure
    'person' => [
        'create' => [
            // here you can specify some validation rules for your sign-in request
            'validation_rules' => [
                'user_id' => 'numeric|exists:users,id',
                // 'name' => 'required',
                // 'email' => 'required|unique:tbl_partner_person',
                // 'idType' => 'alpha_num',
                // 'idNo' => 'numeric|unique:tbl_partner_person',
                // 'latitude' => ['regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
                // 'longitude' => ['regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
                'address01' => '',
                // 'address02' => '',
                'taxNo' => '',
                'phone' => 'required|regex:/^\(?(\+[0-9\-]{1,5})?[-. ]([0-9]{3})\)?[-. ]?([0-9]{2,4})[-. ]?([0-9]{4,5})$/',
                'whatsapp' => 'regex:/^\(?(\+[0-9\-]{1,5})?[-. ]([0-9]{3})\)?[-. ]?([0-9]{2,4})[-. ]?([0-9]{4,5})$/',
                'line' => 'min:4',
                'telegram' => 'regex:/^\(?(\+[0-9\-]{1,5})?[-. ]([0-9]{3})\)?[-. ]?([0-9]{2,4})[-. ]?([0-9]{4,5})$/',
                'contact1' => 'regex:/^\(?(\+[0-9\-]{1,5})?[-. ]([0-9]{3})\)?[-. ]?([0-9]{2,4})[-. ]?([0-9]{4,5})$/',
                // 'email' => 'required|email',
                // 'password' => 'required',
                // // 'phone' => 'required|numeric|regex:/^\(?(0[0-9]{3})\)?[-. ]?([0-9]{2,4})[-. ]?([0-9]{4,5})$/'
                // 'phone' => 'required|min:11|numeric'
            ],
        ],
        'edit' => [
            // here you can specify some validation rules for your sign-in request
            'validation_rules' => [
                // 'name' => 'required',
                // 'email' => 'required',
                'user_id' => 'numeric|exists:users,id',
                // 'idType' => 'alpha_num',
                // 'idNo' => 'numeric',
                // 'latitude' => ['regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
                // 'longitude' => ['regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
                'address01' => '',
                // 'address02' => '',
                'taxNo' => '',
                'phone' => 'required|regex:/^\(?(\+[0-9\-]{1,5})?[-. ]([0-9]{3})\)?[-. ]?([0-9]{2,4})[-. ]?([0-9]{4,5})$/',
                'whatsapp' => 'regex:/^\(?(\+[0-9\-]{1,5})?[-. ]([0-9]{3})\)?[-. ]?([0-9]{2,4})[-. ]?([0-9]{4,5})$/',
                'line' => 'min:4',
                'telegram' => 'regex:/^\(?(\+[0-9\-]{1,5})?[-. ]([0-9]{3})\)?[-. ]?([0-9]{2,4})[-. ]?([0-9]{4,5})$/',
                'contact1' => 'regex:/^\(?(\+[0-9\-]{1,5})?[-. ]([0-9]{3})\)?[-. ]?([0-9]{2,4})[-. ]?([0-9]{4,5})$/',
                'deleted' => 'boolean',
                'visit' => 'boolean',
            ],
        ],
    ],
    'restaurant' => [
        'create' => [
            // here you can specify some validation rules for your sign-in request
            'validation_rules' => [
                // 'person_id' => 'required|numeric',
                'name' => 'required|unique:tbl_partner_restaurant',
                'latitude' => ['required', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
                'longitude' => ['required', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
                'city_id' => 'numeric|exists:tbl_cities,id',
                'email' => 'email',
                'idType' => 'alpha_num',
                'idNo' => 'numeric',
                'address01' => '',
                'address02' => '',
                'taxNo' => 'numeric|min:12',
                'companyNo' => 'numeric|min:12',
                'phone' => 'required|unique:tbl_partner_restaurant|regex:/^\(?(\+[0-9\-]{1,5})?[-. ]([0-9]{3})\)?[-. ]?([0-9]{2,4})[-. ]?([0-9]{4,5})$/',
                'whatsapp' => 'regex:/^\(?(\+[0-9\-]{1,5})?[-. ]([0-9]{3})\)?[-. ]?([0-9]{2,4})[-. ]?([0-9]{4,5})$/',
                'line' => 'min:4',
                'telegram' => 'regex:/^\(?(\+[0-9\-]{1,5})?[-. ]([0-9]{3})\)?[-. ]?([0-9]{2,4})[-. ]?([0-9]{4,5})$/',
                'contact1' => 'regex:/^\(?(\+[0-9\-]{1,5})?[-. ]([0-9]{3})\)?[-. ]?([0-9]{2,4})[-. ]?([0-9]{4,5})$/',
            ],
        ],
        'edit' => [
            // here you can specify some validation rules for your sign-in request
            'validation_rules' => [
                // 'person_id' => 'required|numeric',
                'name' => 'required',
                'latitude' => ['required', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
                'longitude' => ['required', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
                'city_id' => 'numeric|exists:tbl_cities,id',
                'email' => 'email',
                // 'idType' => 'alpha_num',
                // 'idNo' => 'numeric|unique:tbl_partner_restaurant',
                'address01' => '',
                'address02' => '',
                'taxNo' => '',
                'companyNo' => '',
                'phone' => 'required|regex:/^\(?(\+[0-9\-]{1,5})?[-. ]([0-9]{3})\)?[-. ]?([0-9]{2,4})[-. ]?([0-9]{4,5})$/',
                'whatsapp' => 'regex:/^\(?(\+[0-9\-]{1,5})?[-. ]([0-9]{3})\)?[-. ]?([0-9]{2,4})[-. ]?([0-9]{4,5})$/',
                'line' => 'min:4',
                'telegram' => 'regex:/^\(?(\+[0-9\-]{1,5})?[-. ]([0-9]{3})\)?[-. ]?([0-9]{2,4})[-. ]?([0-9]{4,5})$/',
                'contact1' => 'regex:/^\(?(\+[0-9\-]{1,5})?[-. ]([0-9]{3})\)?[-. ]?([0-9]{2,4})[-. ]?([0-9]{4,5})$/',
                // 'approved' => 'required|boolean',
                // 'deleted' => 'required|boolean',
                // 'visit' => 'required|boolean'
            ],
        ],
    ],

    'outlet' => [
        'create' => [
            // here you can specify some validation rules for your sign-in request
            'validation_rules' => [
                // 'person_id' => 'required|numeric',
                'restaurant_id' => 'required|numeric|exists:tbl_partner_restaurant,id',
                'city_id' => 'numeric|exists:tbl_cities,id',
                'name' => 'unique:tbl_partner_outlet,name',
                'latitude' => ['required', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
                'longitude' => ['required', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
                'address01' => '',
                'address02' => '',
                'phone' => 'required|unique:tbl_partner_outlet|regex:/^\(?(\+[0-9\-]{1,5})?[-. ]([0-9]{3})\)?[-. ]?([0-9]{2,4})[-. ]?([0-9]{4,5})$/',
                'whatsapp' => 'regex:/^\(?(\+[0-9\-]{1,5})?[-. ]([0-9]{3})\)?[-. ]?([0-9]{2,4})[-. ]?([0-9]{4,5})$/',
                'line' => 'min:4',
                'telegram' => 'regex:/^\(?(\+[0-9\-]{1,5})?[-. ]([0-9]{3})\)?[-. ]?([0-9]{2,4})[-. ]?([0-9]{4,5})$/',
                'contact1' => 'regex:/^\(?(\+[0-9\-]{1,5})?[-. ]([0-9]{3})\)?[-. ]?([0-9]{2,4})[-. ]?([0-9]{4,5})$/',
            ],
        ],
        'edit' => [
            // here you can specify some validation rules for your sign-in request
            'validation_rules' => [
                'restaurant_id' => 'required|numeric|exists:tbl_partner_restaurant,id',
                'city_id' => 'numeric|exists:tbl_cities,id',
                'name' => 'required',
                'latitude' => ['required', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
                'longitude' => ['required', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
                'address01' => '',
                'address02' => '',
                'phone' => 'required|regex:/^\(?(\+[0-9\-]{1,5})?[-. ]([0-9]{3})\)?[-. ]?([0-9]{2,4})[-. ]?([0-9]{4,5})$/',
                'whatsapp' => 'regex:/^\(?(\+[0-9\-]{1,5})?[-. ]([0-9]{3})\)?[-. ]?([0-9]{2,4})[-. ]?([0-9]{4,5})$/',
                'line' => 'min:4',
                'telegram' => 'regex:/^\(?(\+[0-9\-]{1,5})?[-. ]([0-9]{3})\)?[-. ]?([0-9]{2,4})[-. ]?([0-9]{4,5})$/',
                'contact1' => 'regex:/^\(?(\+[0-9\-]{1,5})?[-. ]([0-9]{3})\)?[-. ]?([0-9]{2,4})[-. ]?([0-9]{4,5})$/',
                // 'approved' => 'required|boolean',
                // 'deleted' => 'required|boolean',
                // 'visit' => 'required|boolean'
            ],
        ],
    ],

    'review' => [
        'create' => [
            // here you can specify some validation rules for your sign-in request
            'validation_rules' => [
                'restaurant_id' => 'required|numeric|exists:tbl_partner_restaurant,id',
                'point' => 'numeric|max:5',
                'caption' => 'max:140',
                'description' => 'required|min:10|max:140',
            ],
        ],
        'edit' => [
            // here you can specify some validation rules for your sign-in request
            'validation_rules' => [
                // 'restaurant_id' => 'required|numeric|exists:tbl_partner_restaurant,id',
                'point' => 'numeric|max:5',
                'caption' => 'max:140',
                'description' => 'required|min:10|max:140',
            ],
        ],
    ],

    // these options are related to the login procedure
    'login' => [

        // here you can specify some validation rules for your login request
        'validation_rules' => [
            'email' => 'required|email',
            'password' => 'required',
        ],
    ],

    // these options are related to the password recovery procedure
    'forgot_password' => [

        // here you can specify some validation rules for your password recovery procedure
        'validation_rules' => [
            'email' => 'required|email',
        ],
    ],

    // these options are related to the password recovery procedure
    'reset_password' => [

        // this option must be set to true if you want to release a token
        // when your user successfully terminates the password reset procedure
        'release_token' => env('PASSWORD_RESET_RELEASE_TOKEN', false),

        // here you can specify some validation rules for your password recovery procedure
        'validation_rules' => [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed',
        ],
    ],

];
