<?php

use App\User;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class PersonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $users = User::all()->pluck('id')->toArray();
        for ($i = 0; $i < 50; $i++) {
            DB::table('tbl_partner_person')->insert([
                'user_id' => $faker->randomElement($users),
                'address01' => $faker->streetAddress,
                'taxNo' => $faker->randomNumber,
                'phone' => $faker->randomNumber,
                'whatsapp' => $faker->randomNumber,
                'line' => $faker->randomNumber,
                'telegram' => $faker->randomNumber,
                'contact1' => $faker->randomNumber,
                'deleted' => 0,
                'visit' => $faker->randomDigitNotNull,
                'created_by' => $faker->randomElement($users),
            ]);
        }
    }
}
