<?php

use App\User;
use App\Model\Partner\tbl_partner_person;
use App\tbl_cities;
use App\Model\Partner\tbl_partner_restaurant;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class RestaurantTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $users = User::all()->pluck('id')->toArray();
        $person = tbl_partner_person::all()->pluck('id')->toArray();
        $city = tbl_cities::all()->pluck('id')->toArray();
        for ($i = 0; $i < 50; $i++) {
            DB::table('tbl_partner_restaurant')->insert([
                'person_id' => $faker->randomElement($person) ,
                'name' => $faker->name,
                'latitude' => $faker->latitude($min = -90, $max = 90),
                'longitude' => $faker->longitude($min = -90, $max = 90),
                'city_id' => $faker->randomElement($city),
                'email' => $faker->email,
                'idType' => str_random(6),
                'idNo' => $faker->randomNumber,
                'address01' => $faker->streetAddress,
                'address02' => $faker->streetAddress,
                'taxNo' => $faker->randomNumber,
                'companyNo' => $faker->randomNumber,
                'phone' => $faker->randomNumber,
                'whatsapp' => $faker->randomNumber,
                'line' => $faker->randomNumber,
                'telegram' => $faker->randomNumber,
                'contact1' => $faker->randomNumber,
                'approved' => $faker->boolean,
                'deleted' => 0,
                'visit' => $faker->randomDigitNotNull,
                'created_by' => $faker->randomElement($users),
            ]);
        }
    }
}
