<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblPartnerPerson extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_partner_person', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable()->unsigned()->index();
            // $table->string('name', 50)->nullable(false);
            // $table->string('email', 255)->nullable()->string()->unique();
            // $table->string('idType', 6)->nullable()->string();
            // $table->string('idNo', 20)->nullable()->string();
            // $table->float('latitude', 10, 8)->nullable();
            // $table->float('longitude', 10, 8)->nullable();
            $table->string('address01', 100)->nullable()->string();
            // $table->string('address02', 100)->nullable()->string();
            $table->string('taxNo', 15)->nullable()->string();
            $table->string('phone', 25)->nullable(false);
            $table->string('whatsapp', 25)->nullable();
            $table->string('line', 50)->nullable();
            $table->string('telegram', 25)->nullable();
            $table->string('contact1', 25)->nullable();
            $table->boolean('deleted')->default(0);
            $table->integer('visit')->unsigned()->default(0);
            $table->integer('created_by')->unsigned()->index()->nullable(false)->nullable(false);
            $table->integer('updated_by')->unsigned()->index()->nullable()->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::create('tbl_partner_restaurant', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('person_id')->nullable(false)->unsigned()->index();
            $table->string('name', 50)->nullable(false);
            $table->float('latitude', 10, 8)->nullable();
            $table->float('longitude', 10, 8)->nullable();
            $table->integer('city_id')->unsigned()->index()->nullable();
            $table->string('email', 255)->nullable()->string()->unique();
            $table->string('idType', 6)->nullable()->string();
            $table->string('idNo', 20)->nullable()->string();
            $table->string('address01', 100)->nullable()->string();
            $table->string('address02', 100)->nullable()->string();
            $table->string('taxNo', 15)->nullable()->string();
            $table->string('companyNo', 12)->nullable()->string();
            $table->string('phone', 25)->nullable(false);
            $table->string('whatsapp', 25)->nullable();
            $table->string('line', 50)->nullable();
            $table->string('telegram', 25)->nullable();
            $table->string('contact1', 25)->nullable();
            $table->boolean('approved')->default(0);
            $table->boolean('deleted')->default(0);
            $table->integer('visit')->unsigned()->default(0);
            $table->integer('created_by')->unsigned()->index()->nullable(false)->nullable(false);
            $table->integer('updated_by')->unsigned()->index()->nullable()->nullable();
            $table->timestamps();

            $table->foreign('person_id')->references('id')->on('tbl_partner_person');
            // Full Text Index
        });

        // DB::statement('ALTER TABLE tbl_partner_restaurant ADD FULLTEXT fulltext_index (name, email, address01, address02, phone)');

        Schema::create('tbl_partner_outlet', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('restaurant_id')->unsigned()->index()->nullable(false);
            $table->integer('city_id')->unsigned()->index()->nullable();
            $table->string('name', 50)->nullable(false);
            $table->float('latitude', 10, 8)->nullable();
            $table->float('longitude', 10, 8)->nullable();
            $table->string('address01', 100)->nullable()->string();
            $table->string('address02', 100)->nullable()->string();
            $table->string('phone', 25)->nullable(false);
            $table->string('whatsapp', 25)->nullable();
            $table->string('line', 50)->nullable();
            $table->string('telegram', 25)->nullable();
            $table->string('contact1', 25)->nullable();
            $table->boolean('deleted')->default(0);
            $table->integer('visit')->unsigned()->default(0);
            $table->integer('created_by')->unsigned()->index()->nullable(false)->nullable(false);
            $table->integer('updated_by')->unsigned()->index()->nullable()->nullable();
            $table->timestamps();

            $table->foreign('restaurant_id')->references('id')->on('tbl_partner_restaurant');
            $table->foreign('city_id')->references('id')->on('tbl_cities');
        });

        Schema::create('tbl_partner_review', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('restaurant_id')->unsigned()->index()->nullable(false);
            $table->integer('point')->unsigned()->default(5);
            $table->string('caption', 500)->nullable();
            $table->string('description', 140)->nullable(false);
            $table->boolean('deleted')->default(0);
            $table->integer('visit')->unsigned()->default(0);
            $table->integer('created_by')->unsigned()->index()->nullable(false)->nullable(false);
            $table->integer('updated_by')->unsigned()->index()->nullable()->nullable();
            $table->timestamps();

            $table->foreign('restaurant_id')->references('id')->on('tbl_partner_restaurant');
        });

        Schema::create('tbl_partner_review_like', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('review_id')->unsigned()->index()->nullable(false);
            $table->boolean('like')->default(1);
            $table->boolean('deleted')->default(0);
            $table->integer('visit')->unsigned()->default(0);
            $table->integer('created_by')->unsigned()->index()->nullable(false)->nullable(false);
            $table->integer('updated_by')->unsigned()->index()->nullable()->nullable();
            $table->timestamps();

            $table->foreign('review_id')->references('id')->on('tbl_partner_review');
        });

        Schema::create('tbl_partner_galery', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('restaurant_id')->unsigned()->index()->nullable(false);
            $table->string('photos_url', 2083)->nullable(false);
            $table->string('name', 50)->nullable();
            $table->float('latitude', 10, 8)->nullable();
            $table->float('longitude', 10, 8)->nullable();
            $table->string('caption', 500)->nullable();
            $table->string('description', 500)->nullable();
            $table->integer('review_id')->unsigned()->index()->nullable(false);
            $table->boolean('profile')->default(0);
            $table->boolean('galery')->default(0);
            $table->boolean('thumb')->default(0);
            $table->boolean('deleted')->default(0);
            $table->integer('visit')->unsigned()->default(0);
            $table->integer('created_by')->unsigned()->index()->nullable(false)->nullable(false);
            $table->integer('updated_by')->unsigned()->index()->nullable()->nullable();
            $table->timestamps();

            $table->foreign('restaurant_id')->references('id')->on('tbl_partner_restaurant');
            $table->foreign('review_id')->references('id')->on('tbl_partner_review');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::dropIfExists('tbl_partner_galery');
        Schema::dropIfExists('tbl_partner_review_like');
        Schema::dropIfExists('tbl_partner_review');
        Schema::dropIfExists('tbl_partner_outlet');
        Schema::dropIfExists('tbl_partner_restaurant');
        Schema::dropIfExists('tbl_partner_person');

        Schema::enableForeignKeyConstraints();
    }
}
