<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Negara
        Schema::create('tbl_countries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50)->nullable();
            $table->string('code', 2)->nullable();
            $table->integer('visit')->unsigned()->default(0);
            $table->timestamps();
        });

        // Provinsi
        Schema::create('tbl_regions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 25)->nullable();
            $table->string('code', 7)->nullable();
            $table->integer('country_id')->unsigned()->index()->nullable();
            $table->integer('visit')->unsigned()->default(0);
            $table->timestamps();

            $table->foreign('country_id')->references('id')->on('tbl_countries');
        });

        // Kota
        Schema::create('tbl_cities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('region_id')->unsigned()->index()->nullable();
            $table->integer('country_id')->unsigned()->index()->nullable();
            $table->float('latitude', 10, 8)->nullable();
            $table->float('longitude', 10, 8)->nullable();
            $table->string('name', 36)->nullable();
            $table->integer('visit')->unsigned()->default(0);
            $table->timestamps();

            $table->foreign('region_id')->references('id')->on('tbl_regions');
            $table->foreign('country_id')->references('id')->on('tbl_countries');
        });

        // Kecamatan
        Schema::create('tbl_districts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('city_id')->unsigned()->index()->nullable();
            $table->string('name')->nullable();
            $table->timestamps();
            $table->integer('visit')->unsigned()->default(0);
            $table->foreign('city_id')->references('id')->on('tbl_cities');
        });

        // Kecamatan desa
        Schema::create('tbl_villages', function (Blueprint $table) {
            $table->string('id', 10);
            $table->integer('district_id')->unsigned()->index()->nullable();
            $table->string('name')->nullable();
            $table->timestamps();
            $table->integer('visit')->unsigned()->default(0);
            $table->foreign('district_id')->references('id')->on('tbl_districts');
        });

        // Full Text Index
        DB::statement('ALTER TABLE tbl_cities ADD FULLTEXT fulltext_index (`name`)');
        DB::statement('ALTER TABLE tbl_regions ADD FULLTEXT fulltext_index (`name`, `code`)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::dropIfExists('tbl_villages');
        Schema::dropIfExists('tbl_districts');
        Schema::dropIfExists('tbl_cities');
        Schema::dropIfExists('tbl_regions');
        Schema::dropIfExists('tbl_countries');

        Schema::enableForeignKeyConstraints();        
    }
}
