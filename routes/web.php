<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// Route::get('reset_password/{token}', ['as' => 'password.reset', function ($token) {
// implement your reset password route here!
// }]);

// Route::get('/', function () {
//     return view('home', ['title' => 'Home']);
// });

Auth::routes();

Route::resource('partner', 'PartnerController');
Route::resource('restaurant', 'RestaurantController');

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::get('/partner/new', 'PartnerController@create')->middleware('auth');
// Route::get('/partner', 'PartnerController@index');
Route::get('/event', 'HomeController@showEvent');
Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');

// Route::post('/partner/new', 'PartnerController@create')->middleware('auth');
