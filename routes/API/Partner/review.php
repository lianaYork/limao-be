<?php

use Dingo\Api\Routing\Router;

/*
*
*   Please check app/Providers/RouteServiceProvider.php
*   if you add new 
*   @Route
/

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['prefix' => 'review'], function (Router $api) {
        $api->get('{id}', 'App\\Http\\Controllers\\Partner\\ReviewController@index');
        $api->get('', 'App\\Http\\Controllers\\Partner\\ReviewController@show');
        $api->post('/add', 'App\\Http\\Controllers\\Partner\\ReviewController@create');
        $api->put('{id}', 'App\\Http\\Controllers\\Partner\\ReviewController@edit');
    });
});
