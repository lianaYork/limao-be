<?php

use Dingo\Api\Routing\Router;

/*
*
*   Please check app/Providers/RouteServiceProvider.php
*   if you add new 
*   @Route
/

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['prefix' => 'review-like'], function (Router $api) {
        $api->get('{id}', 'App\\Http\\Controllers\\Partner\\ReviewLikeController@index');
        $api->get('', 'App\\Http\\Controllers\\Partner\\ReviewLikeController@show');
    });
});
