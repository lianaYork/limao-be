<?php

use Dingo\Api\Routing\Router;

/*
 *
 *   Please check app/Providers/RouteServiceProvider.php
 *   if you add new
 *   @Route
/

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['prefix' => 'person'], function (Router $api) {
        $api->get('/id', 'App\\Http\\Controllers\\Partner\\PersonController@index');
        $api->get('', 'App\\Http\\Controllers\\Partner\\PersonController@show');
        $api->post('/add', 'App\\Http\\Controllers\\Partner\\PersonController@create');
        $api->put('/id', 'App\\Http\\Controllers\\Partner\\PersonController@edit');
        $api->put('/approve', 'App\\Http\\Controllers\\Partner\\PersonController@approve');
    });
});
