<?php

use Dingo\Api\Routing\Router;

/*
*
*   Please check app/Providers/RouteServiceProvider.php
*   if you add new 
*   @Route
/

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['prefix' => 'restaurant'], function (Router $api) {
        $api->get('{id}', 'App\\Http\\Controllers\\Partner\\RestaurantController@index');
        $api->get('', 'App\\Http\\Controllers\\Partner\\RestaurantController@show');
        $api->post('/add', 'App\\Http\\Controllers\\Partner\\RestaurantController@create');
        $api->put('{id}', 'App\\Http\\Controllers\\Partner\\RestaurantController@edit');
    });
});
