<?php

use Dingo\Api\Routing\Router;

/*
*
*   Please check app/Providers/RouteServiceProvider.php
*   if you add new 
*   @Route
/

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['prefix' => 'regions'], function (Router $api) {
        $api->get('{id}', 'App\\Http\\Controllers\\Area\\RegionsController@index');
        $api->get('', 'App\\Http\\Controllers\\Area\\RegionsController@show');
    });
});
