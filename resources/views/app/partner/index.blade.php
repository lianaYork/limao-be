@extends('layouts.app') @section('title', 'Partner') @section('assets')
<link href="{{ asset('css/partner.css') }}" rel="stylesheet"> @endsection @section('content') @if (session('status'))
<div class="ui message">
    <i class="close icon"></i>
    <p>{{ session('status') }}</p>
</div>
@endif @if (session('warning'))
<div class="ui message">
    <i class="close icon"></i>
    <p>{{ session('warning') }}</p>
</div>
@endif
<div class="ui container">
    <div class="ui two stackable column grid">
        <div class="two wide">
            <a id="newPartner" href="{{ route('partner.create') }}">
                <button class="ui labeled icon button primary">
                    <i class="{{isset($message['usageIcon']) ? $message['usageIcon'] : 'plus'}} icon"></i>
                    {{ isset($message['usage']) ? $message['usage'] : 'Join' }}
                </button>
            </a>
            <a id="newPartner" href="{{ url('/restaurant') }}">
                <button class="ui labeled icon green button">
                    <i class="food icon"></i>
                    Restaurant
                </button>
            </a>
        </div>
    </div>
    <div class="ui stackable four column grid">
        @foreach($restaurant as $item)
        <div class="column">

            <div class="ui card">
                <div class="image">
                    <img src="{{ asset('images/diet-food-fresh-247685.jpg') }}">
                </div>
                <div class="content">
                    <a class="header">
                        {{ json_encode($item['name']) }}
                    </a>
                    <div class="meta">
                        <span class="date">
                            {{ json_encode($item['address01']) }}
                        </span>
                    </div>
                    <div class="description">
                        {{ json_encode($item['address02']) }}
                    </div>
                </div>
                <div class="extra content">
                    <a>
                        <i class="user icon"></i>
                    </a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection