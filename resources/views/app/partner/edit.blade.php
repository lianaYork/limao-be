@extends('layouts.app') @section('title', 'Partner') @section('assets')
<link href="{{ asset('css/partner.css') }}" rel="stylesheet">
<link href="{{ asset('css/auth.css') }}" rel="stylesheet"> @endsection @section('content')
<div class="ui container">
    <div class="ui two centered stackable column grid">
        <div class="two wide">
            @if (isset($message['status'])) @if ($message['status'] !== 'ok')
            <div class="ui message">
                <i class="close icon"></i>
                <p>{{ $message['status'] }}</p>
            </div>
            @endif @endif
            <div class="ui loginform form">
                <div class="ui dividing header">
                    {{ __('Partner Info') }}
                </div>
                <div class="ui form">
                    <form method="PUT" action="{{ route('partner.update', $message['data']['id']) }}" accept-charset="UTF-8">
                        {{ csrf_field() }}

                        <div class="form-group inline field row">
                            <label for="address01" class="col-sm-4 col-form-label text-md-right">{{ __('Alamat') }}</label>

                            <div class="col-md-6 ui input">
                                <input id="address01" type="text" class="form-control{{ $errors->has('address01') ? ' is-invalid' : '' }}" name="address01"
                                    value="{{ isset($message['data']['address01']) ? $message['data']['address01'] : old('address01') }}"
                                    required>
                            </div>
                        </div>
                        @if (isset($message['address01']))
                        <span class="invalid-feedback">
                            <strong>{{ implode(" ", $message['address01']) }}</strong>
                        </span>
                        @endif


                        <div class="form-group inline field row">
                            <label for="taxNo" class="col-sm-4 col-form-label text-md-right">{{ __('NPWP') }}</label>

                            <div class="col-md-6 ui input">
                                <input id="taxNo" type="text" class="form-control{{ $errors->has('taxNo') ? ' is-invalid' : '' }}" name="taxNo" value="{{ isset($message['data']['taxNo']) ? $message['data']['taxNo'] : old('taxNo') }}"
                                    required>
                            </div>
                        </div>
                        @if (isset($message['taxNo']))
                        <span class="invalid-feedback">
                            <strong>{{ implode(" ", $message['taxNo']) }}</strong>
                        </span>
                        @endif

                        <div class="form-group inline field row">
                            <label for="phone" class="col-sm-4 col-form-label text-md-right">{{ __('No Telp') }}</label>

                            <div class="col-md-6 ui input">
                                <input id="phone" type="tel" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ isset($message['data']['phone']) ? $message['data']['phone'] : old('phone') }}"
                                    required>
                            </div>
                        </div>
                        @if (isset($message['phone']))
                        <span class="invalid-feedback">
                            <strong>{{ implode(" ", $message['phone']) }}</strong>
                        </span>
                        @endif

                        <div class="form-group inline field row">
                            <label for="whatsapp" class="col-sm-4 col-form-label text-md-right">{{ __('Whatsapp') }}</label>

                            <div class="col-md-6 ui input">
                                <input id="whatsapp" type="tel" class="form-control{{ $errors->has('whatsapp') ? ' is-invalid' : '' }}" name="whatsapp" value="{{ isset($message['data']['whatsapp']) ? $message['data']['whatsapp'] : old('whatsapp') }}"
                                    required>
                            </div>
                        </div>
                        @if (isset($message['whatsapp']))
                        <span class="invalid-feedback">
                            <strong>{{ implode(" ", $message['whatsapp']) }}</strong>
                        </span>
                        @endif

                        <div class="form-group inline field row">
                            <label for="line" class="col-sm-4 col-form-label text-md-right">{{ __('Line') }}</label>

                            <div class="col-md-6 ui input">
                                <input id="line" type="text" class="form-control{{ $errors->has('line') ? ' is-invalid' : '' }}" name="line" value="{{ isset($message['data']['line']) ? $message['data']['line'] : old('line') }}"
                                    required>
                            </div>
                        </div>
                        @if (isset($message['line']))
                        <span class="invalid-feedback">
                            <strong>{{ implode(" ", $message['line']) }}</strong>
                        </span>
                        @endif

                        <div class="form-group inline field row">
                            <label for="telegram" class="col-sm-4 col-form-label text-md-right">{{ __('Telegram') }}</label>

                            <div class="col-md-6 ui input">
                                <input id="telegram" type="text" class="form-control{{ $errors->has('telegram') ? ' is-invalid' : '' }}" name="telegram"
                                    value="{{ isset($message['data']['telegram']) ? $message['data']['telegram'] : old('telegram') }}"
                                    required>
                            </div>
                        </div>
                        @if (isset($message['telegram']))
                        <span class="invalid-feedback">
                            <strong>{{ implode(" ", $message['telegram']) }}</strong>
                        </span>
                        @endif

                        <div class="form-group inline field row">
                            <label for="contact1" class="col-sm-4 col-form-label text-md-right">{{ __('Other contact') }}</label>

                            <div class="col-md-6 ui input">
                                <input id="contact1" type="text" class="form-control{{ $errors->has('contact1') ? ' is-invalid' : '' }}" name="contact1"
                                    value="{{ isset($message['data']['contact1']) ? $message['data']['contact1'] : old('contact1') }}"
                                    required>
                            </div>
                        </div>
                        @if (isset($message['contact1']))
                        <span class="invalid-feedback">
                            <strong>{{ implode(" ", $message['contact1']) }}</strong>
                        </span>
                        @endif
                        <div class="field">
                            <button type="text" class="ui button primary  btn btn-primary">
                                {{ __('Edit') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection @section('js')
<script type="text/javascript">
    $('.message .close')
        .on('click', function () {
            $(this)
                .closest('.message')
                .transition('fade')
                ;
        })
        ;
</script> @endsection