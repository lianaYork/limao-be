<!DOCTYPE html>
<html lang="en">

<head>
  <!-- ogp tag -->
  <meta property="og:type" content="website" />
  <!-- write current url -->
  <meta property="og:site_name" content="Limaofood" />
  <meta property="og:title" content="Cari Info Kuliner" />
  <meta property="og:description" content="Limaofood menyediakan info restoran dan promo yang lengkap. Temukan restoran yang enak, restoran yang romantis, chinese food yang enak, steak yang enak menggunakan limaofood! Anda juga dapat menggunakan filter untuk membantu mencari restoran."
  />

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta content="Best Food Website" name="description" />
  <meta content="AIinterface" name="author" />

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>@yield('title') | Limaofood: All you can eat</title>
  <link rel="icon" href="{{ asset('images/logo16x16.jpg') }}" />
  <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4"
    crossorigin="anonymous"> -->
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/semantic-ui/semantic.min.css')}} ">
  <script src="{{ asset('assets/jquery/jquery.min.js') }}"></script>
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <style>
    @import url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
    .logo {
      color: #ffffff;
    }

    .navbar-bg {
      background-color: #4CAF50;
    }

    .body {
      font-family: monospace, 'Roboto', sans-serif;
      padding-top: 70px;
    }

    .container {
      width: 100%;
      color: #4CAF50;
    }

    .nav-link {
      color: #ffffff;
      border-radius: 3px;
      margin: 0 4px;
    }

    .nav-link:hover {
      background-color: #ffffff;
      color: #4CAF50;
    }

    .auth {
      color: #ffffff;
    }

    .signup {
      border-style: dashed;
      border-width: 1px;
    }

    .auth:hover {
      color: #4CAF50;
    }

    .navbar-toggle {
      border-color: #ffffff;
    }

    .dropdown-item:hover {
      background-color: #4CAF50;
    }

    /* Footer */

    section {
      padding: 60px 0;
    }

    section .section-title {
      text-align: center;
      color: #4CAF50;
      margin-bottom: 50px;
      text-transform: uppercase;
    }

    #footer {
      background: #4CAF50 !important;
      position: inherit;
      bottom: 0;
      width: 100%;
    }

    #footer h5 {
      padding-left: 10px;
      border-left: 3px solid #eeeeee;
      padding-bottom: 6px;
      margin-bottom: 20px;
      color: #ffffff;
    }

    #footer a {
      color: #ffffff;
      text-decoration: none !important;
      background-color: transparent;
      -webkit-text-decoration-skip: objects;
    }

    #footer ul.social li {
      padding: 3px 0;
    }

    #footer ul.social li a i {
      margin-right: 5px;
      font-size: 25px;
      -webkit-transition: .5s all ease;
      -moz-transition: .5s all ease;
      transition: .5s all ease;
    }

    #footer ul.social li:hover a i {
      font-size: 30px;
      margin-top: -10px;
    }

    #footer ul.social li a,
    #footer ul.quick-links li a {
      color: #ffffff;
    }

    #footer ul.social li a:hover {
      color: #eeeeee;
    }

    #footer ul.quick-links li {
      padding: 3px 0;
      -webkit-transition: .5s all ease;
      -moz-transition: .5s all ease;
      transition: .5s all ease;
    }

    #footer ul.quick-links li:hover {
      padding: 3px 0;
      margin-left: 5px;
      font-weight: 700;
    }

    #footer ul.quick-links li a i {
      margin-right: 5px;
    }

    #footer ul.quick-links li:hover a i {
      font-weight: 700;
    }

    @media (max-width:767px) {
      #footer h5 {
        padding-left: 0;
        border-left: transparent;
        padding-bottom: 0px;
        margin-bottom: 10px;
      }
    }
  </style>
</head>

<body>
  <div class="ui menu navbar navbar-bg navbar-expand-lg">
    <div class="container">
      <img src="{{ asset('images/logo-white.svg') }}" height="45px" width="45px" />
      <h2 class="logo">Limaofood</h2>
      <button class="navbar-toggle navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive"
        aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon">
        </span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          @foreach(tbl_menu::tree() as $item) @if (count($item['children']) > 0)

          <li class="nav-item active dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              {{ $item->name }}
            </a>
            @else
            <li class="nav-item">
              <a class="nav-link" href="{{ url($item->url) }}">{{ $item->name }}</a>
              @endif
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                @foreach($item['children'] as $child)
                <a class="dropdown-item" href="{{ url($child->url) }}">{{ $child->name }}</a>
                @endforeach
              </div>

            </li>
            @endforeach @if (Auth::guest())
            <!-- Authentication Links -->
            <li class="nav-item">
              <a class="nav-link auth btn-primary" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            <li class="nav-item">
              <a class="nav-link signup selected" href="{{ route('register') }}">{{ __('Register') }}</a>
            </li>
            @else
            <li class="nav-item dropdown">
              <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                aria-expanded="false" v-pre>
                {{ Auth::user()->name }}
                <span class="caret"></span>
              </a>

              <div class="dropdown-menu auth" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                  {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
                </form>
              </div>
            </li>
            @endif
        </ul>
      </div>
    </div>
  </div>
  <div>
    @section('sidebar')
    <!-- This is the master sidebar. -->
    <div class="container">
      @yield('content')
    </div>
  </div>

  <!-- Footer -->
  <section id="footer">
    <div class="container">
      <div class="row text-center text-xs-center text-sm-left text-md-left">
        <div class="col-xs-12 col-sm-4 col-md-4">
          <h5>Links</h5>
          <ul class="list-unstyled quick-links">
            <li>
              <a href="{{ url('home') }}">
                <i class="fa fa-angle-double-right"></i>Home</a>
            </li>
            <li>
              <a href="{{ url('partner') }}">
                <i class="fa fa-angle-double-right"></i>Partner</a>
            </li>
            <li>
              <a href="{{ url('event') }}">
                <i class="fa fa-angle-double-right"></i>Event</a>
            </li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-5">
          <ul class="list-unstyled list-inline social text-center">
            <li class="list-inline-item">
              <a href="https://www.facebook.com/LIMAO-FOOD-200210860570227/" target="_blank">
                <i class="fa fa-facebook"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="https://www.instagram.com/limaofood/" target="_blank">
                <i class="fa fa-instagram"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="mailto:cs.limaofood@gmail.com" target="_blank">
                <i class="fa fa-google-plus"></i>
              </a>
            </li>
          </ul>
        </div>
        </hr>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
          <p>Limaofood is the best for your food references</p>
          <p class="h6">&copy 2018 All right Reversed.
            <a class="text-green ml-2" href="{{ url('home') }}" target="_blank">Limaofood</a>
          </p>
        </div>
        </hr>
      </div>
    </div>
  </section>
  <!-- ./Footer -->
</body>

</html>