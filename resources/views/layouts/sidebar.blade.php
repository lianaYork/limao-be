<!-- Following Menu -->
<div class="ui large top fixed hidden menu">
    <div class="ui container">
        @foreach(tbl_menu::tree() as $item)
        <a class="item" href="{{ url($item->url) }}">
            <h4 class="logo">
                {{ $item->name }}
            </h4>
        </a>
        @endforeach @if (Auth::guest())
        <!-- Authentication Links -->
        <div class="item logo">
            <a href="{{ url('/login') }}">
                <h3>
                    {{ __('Login') }}
                </h3>
            </a>
        </div>
        <div class="item logo">
            <a href="{{ route('register') }}">
                <h3>
                    {{ __('Register') }}
                </h3>
            </a>
        </div>
        @else

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
        <div class="item">
            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                <h4>
                    {{ __('Logout') }}
                </h4>
            </a>
        </div>
        @endif
    </div>
</div>

<!-- Sidebar Menu -->
<div class="ui vertical inverted sidebar menu">
    @foreach(tbl_menu::tree() as $item)
    <a class="item" href="{{ url($item->url) }}">
        <h4 class="logo">
            {{ $item->name }}
        </h4>
    </a>
    @endforeach @if (Auth::guest())
    <!-- Authentication Links -->
    <div class="item logo">
        <a href="{{ url('/login') }}">
            <h3>
                {{ __('Login') }}
            </h3>
        </a>
    </div>
    <div class="item logo">
        <a href="{{ route('register') }}">
            <h3>
                {{ __('Register') }}
            </h3>
        </a>
    </div>
    @else

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
    <div class="item">
        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
            <h4>
                {{ __('Logout') }}
            </h4>
        </a>
    </div>
    @endif
</div>