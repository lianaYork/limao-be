<div class="ui inverted vertical footer segment">
    <div class="ui container">
        <div class="ui stackable inverted divided equal height stackable grid">
            <div class="three wide column">
                <h4 class="ui inverted header">About</h4>
                <div class="ui inverted link list">
                    @foreach(tbl_menu::tree() as $item)
                    <a class="item" href="{{ url($item->url) }}">
                        {{ $item->name }}
                    </a>
                    @endforeach
                </div>
            </div>
            <div class="seven wide column">
                <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
                    <p>Limaofood is the best for your food references</p>
                    <p class="h6">&copy 2018 All right Reversed.
                        <a class="text-green ml-2" href="{{ url('home') }}">Limaofood</a>
                    </p>
                </div>
                <hr />
            </div>
        </div>
    </div>
</div>