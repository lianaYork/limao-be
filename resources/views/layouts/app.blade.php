<!DOCTYPE html>
<html lang="en">

<head>
    <!-- ogp tag -->
    <meta property="og:type" content="website" />
    <!-- write current url -->
    <meta property="og:site_name" content="Limaofood" />
    <meta property="og:title" content="Cari Info Kuliner" />
    <meta property="og:description" content="Limaofood menyediakan info restoran dan promo yang lengkap. Temukan restoran yang enak, restoran yang romantis, chinese food yang enak, steak yang enak menggunakan limaofood! Anda juga dapat menggunakan filter untuk membantu mencari restoran."
    />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="Best Food Website" name="description" />
    <meta content="AIinterface" name="author" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') | Limaofood: All you can eat</title>
    @yield('assets')
    <link rel="icon" href="{{ asset('images/logo16x16.jpg') }}" />
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/semantic-ui/semantic.min.css')}} ">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/semantic-ui/semantic.min.js')}} ">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/jquery/jquery.min.js')}} "> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.9/semantic.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.9/semantic.min.js"></script>
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4"
        crossorigin="anonymous"> -->
    <!-- <script src="{{ asset('assets/jquery/jquery.min.js') }}"></script> -->
    <link href="{{ asset('css/home.css') }}" rel="stylesheet">
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script> -->
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <script>
        $(document)
            .ready(function () {
                // fix menu when passed
                $('.masthead')
                    .visibility({
                        once: false,
                        onBottomPassed: function () {
                            $('.fixed.menu').transition('fade in');
                        },
                        onBottomPassedReverse: function () {
                            $('.fixed.menu').transition('fade out');
                        }
                    })
                    ;
                // create sidebar and attach to menu open
                $('.ui.sidebar')
                    .sidebar('attach events', '.toc.item')
                    ;
            })
            ;
    </script>
</head>

<body>
    @include('layouts.sidebar')
    <!-- Page Contents -->
    <div class="pusher">
        <!-- Site content !-->
        <div class="ui inverted vertical masthead center aligned segment ">
            <div class="ui container ">
                <div class="ui large secondary inverted pointing menu ">
                    <a class="toc item ">
                        <i class="sidebar icon "></i>
                    </a>
                    <div class="toc">
                        <a href="{{ url( '/') }} ">
                            <img src="{{ asset( 'images/logo-white.svg') }} " height="45px " width="45px " />
                        </a>
                    </div>
                    <div class="toc logo ">
                        <a href="{{ url( '/') }} ">
                            <h2 class="logo ">
                                Limaofood
                            </h2>
                        </a>
                    </div>
                    @foreach(tbl_menu::tree() as $item)
                    <a class="item " href="{{ url($item->url) }}">
                        <h4 class="logo">
                            {{ $item->name }}
                        </h4>
                    </a>
                    @endforeach
                    <div class="right item">
                        <div class="item">
                            <div class="ui icon input">
                                <input type="text" placeholder="Search...">
                                <i class="search link icon"></i>
                            </div>
                        </div>
                        @if (Auth::guest())
                        <!-- Authentication Links -->
                        <div class="item logo">
                            <a href="{{ url('/login') }}">
                                <h3>
                                    {{ __('Login') }}
                                </h3>
                            </a>
                        </div>
                        <div class="item logo">
                            <a href="{{ route('register') }}">
                                <h3>
                                    {{ __('Register') }}
                                </h3>
                            </a>
                        </div>
                        @else

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                        <div class="item">
                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                <h4>
                                    {{ __('Logout') }}
                                </h4>
                            </a>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>


        <div class="ui vertical stripe segment">
            <div class="ui container">
                <div>
                    <!-- This is the master sidebar. -->
                    @yield('content')
                </div>
            </div>
        </div>


        @include('layouts.footer')
    </div>
    @yield('js')
    <script>
        function scrollWin() {
            window.scrollTo(500, 0);
        }
    </script>
</body>

</html>