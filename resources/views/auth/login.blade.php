@extends('layouts.app') @section('title', 'Login') @section('content') @section('assets')
<link href="{{ asset('css/auth.css') }}" rel="stylesheet"> @endsection
<div class="ui two column centered grid">
    <div class="nine wide">
        @if (session('status'))
        <div class="ui message">
            <i class="close icon"></i>
            <p>{{ session('status') }}</p>
        </div>
        @endif @if (session('warning'))
        <div class="ui message">
            <i class="close icon"></i>
            <p>{{ session('warning') }}</p>
        </div>
        @endif
        <div class="ui loginform form">
            <div class="card">
                <div class="ui dividing header">{{ __('Login') }}</div>
                <div class="ui form">
                    <form method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group field row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6 ui input">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}"
                                    required autofocus>
                            </div>
                        </div>
                        @if ($errors->has('email'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                        <br />
                        <div class="form-group field row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6 ui input">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"
                                    required>
                            </div>
                        </div>
                        @if ($errors->has('password'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif

                        <!-- <div class="field">
                                    <div class="col-md-6 offset-md-4">
                                        <div class="ui checkbox">
                                            <input type="checkbox" name="remember" {{ old( 'remember') ? 'checked' : '' }}>
                                            <label>
                                                {{ __('Remember Me') }}
                                            </label>
                                        </div>
                                    </div>
                                </div> -->

                        <div class="ui field form-group mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="ui button primary  btn btn-primary">
                                    {{ __('Login') }}
                                </button>
                                <a class="ui button btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                                <a class="ui button btn btn-link" href="{{ url('/register') }}">
                                    {{ __('Signup') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection @section('js')
<script type="text/javascript">
    $('.message .close')
        .on('click', function () {
            $(this)
                .closest('.message')
                .transition('fade')
                ;
        })
        ;
</script> @endsection