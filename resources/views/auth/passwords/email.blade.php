@extends('layouts.app') @section('title', 'Forget') @section('assets')
<link href="{{ asset('css/auth.css') }}" rel="stylesheet"> @endsection @section('content')
<div class="ui two column centered grid">
    <div class="nine wide">
        @if (session('status'))
        <div class="ui message">
            <i class="close icon"></i>
            <p>{{ session('status') }}</p>
        </div>
        @endif @if (session('warning'))
        <div class="ui message">
            <i class="close icon"></i>
            <p>{{ session('warning') }}</p>
        </div>
        @endif
        <div class="ui form loginform">
            <div class="ui dividing header">{{ __('Reset Password') }}</div>

            <div class="card-body">
                <form method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}

                    <div class="field form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}"
                                required> @if ($errors->has('email'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="ui button primary">
                                {{ __('Send Password Reset Link') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection @section('js')
<script type="text/javascript">
    $('.message .close')
        .on('click', function () {
            $(this)
                .closest('.message')
                .transition('fade')
                ;
        })
        ;
</script> @endsection