@extends('layouts.app') @section('title', 'Home') @section('content')
<div class="ui stackable four column grid">
    @foreach($restaurant as $item)
    <div class="column">

        <div class="ui card">
            <div class="image">
                <img src="{{ asset('images/diet-food-fresh-247685.jpg') }}">
            </div>
            <div class="content">
                <a class="header">
                    {{ json_encode($item['name']) }}
                </a>
                <div class="meta">
                    <span class="date">
                        {{ json_encode($item['address01']) }}
                    </span>
                </div>
                <div class="description">
                    {{ json_encode($item['address02']) }}
                </div>
            </div>
            <div class="extra content">
                <a>
                    <i class="user icon"></i>
                </a>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection