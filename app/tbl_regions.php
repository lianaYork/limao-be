<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\FullTextSearch;

class tbl_regions extends Model
{
    use FullTextSearch;

    // Relationship tbl_countries
    public function countries()
    {
        return $this->hasOne(tbl_countries::class);
    }

    protected $searchable = [
        'name',
        'code'
    ];

    protected $fillable = [
        'name', 'code', 'country_id', 'created_at',
    ];

}
