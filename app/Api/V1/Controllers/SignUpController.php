<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Requests\SignUpRequest;
use App\Http\Controllers\Controller;
use App\Mail\VerifyMail;
use App\User;
use App\VerifyUser;
use Config;
use DB;
use Mail;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\JWTAuth;

class SignUpController extends Controller
{
    public function signUp(SignUpRequest $request, JWTAuth $JWTAuth)
    {
        $email = $request->get('email');

        $count = DB::table('users')
            ->where('email', '=', $email)
            ->count();

        if ($count > 0) {
            return response()->json([
                'status' => 'failed',
                'ok' => false,
                'count' => $count,
                'message' => 'Email already taken',
            ], 422);
        } else {
            $user = new User($request->all());
            if (!$user->save()) {
                throw new HttpException(500);
            }

            if (!Config::get('boilerplate.sign_up.release_token')) {
                return response()->json([
                    'status' => 'ok',
                ], 201);
            }

            $token = $JWTAuth->fromUser($user);

            // verify user send email
            $verifyUser = VerifyUser::create([
                'user_id' => $user->id,
                'token' => str_random(40),
            ]);
            Mail::to($user->email)->send(new VerifyMail($user));
            return response()->json([
                'status' => 'ok',
                'ok' => true,
                'token' => $token,
            ], 201);
        }

    }
}
