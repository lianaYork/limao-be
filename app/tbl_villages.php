<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_villages extends Model
{
    protected $searchable = [
        'name',
    ];

    protected $fillable = [
        'district_id', 'name'
    ];
}
