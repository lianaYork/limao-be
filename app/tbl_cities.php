<?php

namespace App;

use App\Traits\FullTextSearch;
use Illuminate\Database\Eloquent\Model;

class tbl_cities extends Model
{
    use FullTextSearch;

    public function countries()
    {
        return $this->hasOne(tbl_citites::class);
    }

    protected $searchable = [
        'name',
    ];

    protected $fillable = [
        'region_id', 'country_id', 'longitude', 'latitude', 'name', 'created_at',
    ];
    
}
