<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use View;

class HomeController extends BaseController
{
    protected $layout = "layouts";

    public function index()
    {
        $client = new \GuzzleHttp\Client([
            'base_uri' => env('APP_URL', 'http://localhost'),
            'defaults' => [
                'exceptions' => false,
            ],
        ]);
        $request = $client->get('/api/restaurant?strict=false&q[name]=&type=or&page=3', ['Accept' => 'application/json']);
        if ($request->getStatusCode() === 200) {
            $response = (string) $request->getBody();
            return view('home')->with('restaurant', json_decode($response, true)['data']);
        }
        // $response = $client->get('/api/restaurant?strict=false&q[name]=El&type=or');
        // return view('home')->with('restaurant', $response->getBody());
    }

    public function showPartner()
    {
        $client = new \GuzzleHttp\Client([
            'base_uri' => env('APP_URL', 'http://localhost'),
            'defaults' => [
                'exceptions' => false,
            ],
        ]);
        $request = $client->get('/api/restaurant?strict=false&q[name]=&type=or&page=3', ['Accept' => 'application/json']);
        if ($request->getStatusCode() === 200) {
            $response = (string) $request->getBody();
            return view('partner')->with('restaurant', json_decode($response, true)['data']);
        }
    }

    public function showNewPartner()
    {
        return view('newPartner');
    }

    public function showEvent()
    {
        $client = new \GuzzleHttp\Client([
            'base_uri' => env('APP_URL', 'http://localhost'),
            'defaults' => [
                'exceptions' => false,
            ],
        ]);
        $request = $client->get('/api/restaurant?strict=false&q[name]=&type=or&page=3', ['Accept' => 'application/json']);
        if ($request->getStatusCode() === 200) {
            $response = (string) $request->getBody();
            return view('event')->with('restaurant', json_decode($response, true)['data']);
        }
    }
}
