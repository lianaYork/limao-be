<?php

namespace App\Http\Controllers;

use App\Model\Partner\tbl_partner_person;
use Auth;
use Cookie;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use View;

class PartnerController extends BaseController
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $client = new \GuzzleHttp\Client([
            'base_uri' => env('APP_URL', 'http://localhost'),
            'defaults' => [
                'exceptions' => false,
            ],
        ]);
        $request = $client->get('/api/restaurant?strict=false&q[name]=&type=or&page=3', ['Accept' => 'application/json']);
        if ($request->getStatusCode() === 200) {
            $response = (string) $request->getBody();
        }
        $user_id = Auth::guard()->user()['id'];
        try {
            $client = new \GuzzleHttp\Client([
                'base_uri' => env('APP_URL', 'http://localhost'),
                'defaults' => [
                    'exceptions' => false,
                ],
            ]);
            $req = $client->request('GET', '/api/person/id', [
                'headers' => [
                    'Authorization' => 'Bearer ' . Cookie::get('token'),
                ],
            ]
            );

            if (json_decode($req->getBody(), true)['ok'] === true) {
                return view('app.partner.index')->with('message', ['usage' => 'Profile', 'usageIcon' => 'user'])->with('restaurant', json_decode($response, true)['data']);
            }
        } catch (BadResponseException $e) {
            $res = $e->getResponse()->getBody();
            return view('app.partner.index')->with('message', ['usage' => 'Join', 'usageIcon' => 'plus'])->with('restaurant', json_decode($response, true)['data']);
        }

        // if ($request->getStatusCode() === 200) {
        //     $response = (string) $request->getBody();
        //     return view('app.partner.index')->with('restaurant', json_decode($response, true)['data']);
        // }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_id = Auth::guard()->user()['id'];
        $user_name = Auth::guard()->user()['name'];
        try {
            $client = new \GuzzleHttp\Client([
                'base_uri' => env('APP_URL', 'http://localhost'),
                'defaults' => [
                    'exceptions' => false,
                ],
            ]);
            $req = $client->request('GET', '/api/person/id', [
                'headers' => [
                    'Authorization' => 'Bearer ' . Cookie::get('token'),
                ],
            ]
            );
            if (json_decode($req->getBody(), true)['ok'] === true) {
                return view('app.partner.edit')->with('message', ['status' => 'Hi, ' . $user_name, 'data' => json_decode($req->getBody(), true)['data']]);
            }
        } catch (BadResponseException $e) {
            $response = $e->getResponse()->getBody();
            return view('app.partner.create')->with('message', '');
        }
        return view('app.partner.create')->with('message', '');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $client = new \GuzzleHttp\Client([
                'base_uri' => env('APP_URL', 'http://localhost'),
                'defaults' => [
                    'exceptions' => false,
                ],
            ]);
            $req = $client->request('POST', '/api/person/add', [
                'headers' => [
                    'Authorization' => 'Bearer ' . Cookie::get('token'),
                    'Content-type' => 'application/json',
                ],
                'body' => json_encode($request->only(
                    [
                        'address01',
                        'taxNo',
                        'phone',
                        'whatsapp',
                        'line',
                        'telegram',
                        'contact1',
                    ]
                )),
            ]
            );
            if ($req->getStatusCode() === 200) {
                return redirect()->route('partner.create')->with('message', json_decode('Personal Data created successfully', true));
            }
        } catch (BadResponseException $e) {
            $response = $e->getResponse()->getBody();
            return view('app.partner.create')
                ->with('message',
                    isset(json_decode($response, true)['error']) ?
                    json_decode($response, true)['error']['errors'] :
                    ['status' => json_decode($response, true)['message']]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user_name = Auth::guard()->user()['name'];
        $person = tbl_partner_person::where('id', '=', $id)->first();
        return view('app.partner.edit')->with('message', ['status' => 'Hi, ' . $user_name, 'data' => $person]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_id = Auth::guard()->user()['id'];
        try {
            $client = new \GuzzleHttp\Client([
                'base_uri' => env('APP_URL', 'http://localhost'),
                'defaults' => [
                    'exceptions' => false,
                ],
            ]);
            $req = $client->request('PUT', '/api/person/id', [
                'headers' => [
                    'Authorization' => 'Bearer ' . Cookie::get('token'),
                    'Content-type' => 'application/json',
                ],
                'body' => json_encode($request->only(
                    [
                        'address01',
                        'taxNo',
                        'phone',
                        'whatsapp',
                        'line',
                        'telegram',
                        'contact1',
                    ]
                )),
            ]
            );
            if ($req->getStatusCode() === 200) {
                return redirect()->route('partner.create')->with('message', ['status' => 'Personal Data created successfully']);
            }
        } catch (BadResponseException $e) {
            $response = $e->getResponse()->getBody();
            return view('app.partner.create')
                ->with('message',
                    isset(json_decode($response, true)['error']) ?
                    json_decode($response, true)['error']['errors'] :
                    ['status' => json_decode($response, true)['message']]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
