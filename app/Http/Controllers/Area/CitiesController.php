<?php

namespace App\Http\Controllers\Area;

use App\tbl_cities;
use App\tbl_regions;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CitiesController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index']]);
    }
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $params = $request->route('id');
        $results = tbl_cities::where('id', $params)->first();
        if(!$results) {
            throw new NotFoundHttpException();
        }
        return response()->json([
            'status' => true,
            'params' => $params,
            'result' => $results
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return tbl_cities::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        // Query Builder
        $region_id = $request->get('region_id');
        $city_name = $request->get('name');
        $region_name = tbl_regions::where('id', $region_id)->pluck('name')->toArray();

        $results = tbl_cities::where([
            ['region_id', '=', $region_id],
            ['name', 'like', '%' . $city_name . '%']
        ])->paginate(10)->toArray();
        
        // Check results
        if(!$results || !$region_id) {
            throw new NotFoundHttpException();
        }

        // Return Response
        return response()->json([
            'status' => 'ok',
            'success'=>true,
            'region_name' => implode($region_name),
            'page' => $results['current_page'],
            'pageSize' => $results['per_page'],
            'last' => $results['last_page'],
            'total' => $results['total'],
            'data' => $results['data']
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // $results = tbl_cities::findOrFail($id);
        // $results->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // $results = tbl_cities::findOrFail($id);
        // $results->delete();

        return 204;
    }
}
