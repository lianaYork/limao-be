<?php

namespace App\Http\Controllers\Area;

use App\tbl_villages;
use App\tbl_cities;
use Illuminate\Http\Request;

class VillagesController extends Controller
{
    //
    //
    public function index(Request $request)
    {
        $params = $request->route('id');
        $results = tbl_villages::where('id', $params)->first();
        if(!$results) {
            throw new NotFoundHttpException();
        }
        return response()->json([
            'status' => true,
            'params' => $params,
            'result' => $results
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return tbl_villages::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show(Request $request)
    {
        // Query Builder
        $district_id = $request->get('district_id');
        $villages_name = $request->get('name');
        $district_name = tbl_cities::where('id', $villages_name)->pluck('name')->toArray();

        $results = tbl_villages::where([
            ['district_id', '=', $district_id],
            ['name', 'like', '%' . $villages_name . '%'],
        ])->paginate(10)->toArray();
        
        // Check results
        if(!$results || !$district_id) {
            throw new NotFoundHttpException();
        }

        // Return Response
        return response()->json([
            'status' => 'ok',
            'success'=>true,
            'district_name' => implode($district_name),
            'page' => $results['current_page'],
            'pageSize' => $results['per_page'],
            'last' => $results['last_page'],
            'total' => $results['total'],
            'data' => $results['data']
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $results = tbl_villages::findOrFail($id);
        $results->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $results = tbl_villages::findOrFail($id);
        $results->delete();

        return 204;
    }
}
