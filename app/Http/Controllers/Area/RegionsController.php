<?php

namespace App\Http\Controllers\Area;

use App\tbl_countries;
use App\tbl_regions;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RegionsController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index']]);
    }
    
    //
    public function index(Request $request)
    {
        $params = $request->route('id');
        $results = tbl_regions::where('id', $params)->first();
        if(!$results) {
            throw new NotFoundHttpException();
        }
        return response()->json([
            'status' => true,
            'params' => $params,
            'result' => $results
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return tbl_regions::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show(Request $request)
    {
        // Query Builder
        $country_id = $request->get('country_id');
        $region_name = $request->get('name');
        $country_name = tbl_countries::where('id', $country_id)->pluck('name')->toArray();

        $results = tbl_regions::where([
            ['country_id', '=', $country_id],
            ['name', 'like', '%' . $region_name . '%'],
            ['code', 'like', '%' . $region_name . '%', 'or']
        ])->paginate(10)->toArray();
        
        // Check results
        if(!$results || !$country_id) {
            throw new NotFoundHttpException();
        }

        // Return Response
        return response()->json([
            'status' => 'ok',
            'success'=>true,
            'country_name' => implode($country_name),
            'page' => $results['current_page'],
            'last' => $results['last_page'],
            'pageSize' => $results['per_page'],
            'total' => $results['total'],
            'data' => $results['data']
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $results = tbl_regions::findOrFail($id);
        $results->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $results = tbl_regions::findOrFail($id);
        $results->delete();

        return 204;
    }
}
