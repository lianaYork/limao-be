<?php

namespace App\Http\Controllers\Area;

use App\tbl_cities;
use App\tbl_districts;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DistrictsController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index']]);
    }
    
    //
    public function index(Request $request)
    {
        $params = $request->route('id');
        $results = tbl_districts::where('id', $params)->first();
        if(!$results) {
            throw new NotFoundHttpException();
        }
        return response()->json([
            'status' => true,
            'params' => $params,
            'result' => $results
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return tbl_districts::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show(Request $request)
    {
        // Query Builder
        $city_id = $request->get('city_id');
        $district_name = $request->get('name');
        $city_name = tbl_cities::where('id', $district_name)->pluck('name')->toArray();

        $results = tbl_districts::where([
            ['city_id', '=', $city_id],
            ['name', 'like', '%' . $district_name . '%'],
        ])->paginate(10)->toArray();
        
        // Check results
        if(!$results || !$city_id) {
            throw new NotFoundHttpException();
        }

        // Return Response
        return response()->json([
            'status' => 'ok',
            'success'=>true,
            'city_name' => implode($city_name),
            'page' => $results['current_page'],
            'pageSize' => $results['per_page'],
            'last' => $results['last_page'],
            'total' => $results['total'],
            'data' => $results['data']
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $results = tbl_districts::findOrFail($id);
        $results->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $results = tbl_districts::findOrFail($id);
        $results->delete();

        return 204;
    }
}
