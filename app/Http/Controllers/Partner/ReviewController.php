<?php

namespace App\Http\Controllers\Partner;

use Auth;
use App\Model\Partner\tbl_partner_person;
use App\Model\Partner\tbl_partner_restaurant;
use App\Model\Partner\tbl_partner_review;
use App\Http\Requests\Partner\ReviewRequest;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ReviewController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params = $request->route('id');
        $results = tbl_partner_review::
        where('id', $params)
        ->where('deleted', '=', 0)
        ->first();
        if(!$results) {
            throw new NotFoundHttpException();
        }
        return response()->json([
            'status' => true,
            'params' => $params,
            'result' => $results
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ReviewRequest $request)
    {
        // id user yg sedang login
        $auth_id = Auth::guard()->user()['id'];
        
        // check validasi input
        $data = new tbl_partner_review($request->all());
        $count = tbl_partner_review::
                                where('restaurant_id', '=', $request->get('restaurant_id'))
                                ->where('deleted', '=', 0)
                                ->count();
        if ($count >= 1) {
            return response()->json([
                'status' => 'failed',
                'ok' => false,
                'count' => $count,
                'message' => 'You already have review',
            ], 422);
        }
        $person = tbl_partner_person::
        where('user_id', '=', $auth_id)
        // ->where('created_by', '=', $auth_id, 'or')
        ->pluck('id')
        ->toArray();
        if (empty($person)) {
            return response()->json([
                'status' => 'failed',
                'ok' => false,
                'message' => 'You do not have personal data',
            ], 422);
        }
        $data['created_by'] = $auth_id;
        // $authorize_edit = tbl_partner_restaurant::
        //                         where('id', '=', $request->get('restaurant_id'))
        //                         ->where('deleted', '=', 0)
        //                         ->count() || 0;
        // if ($authorize_edit = 0) {
        //     return response()->json([
        //         'status' => 'failed',
        //         'ok' => false,
        //         'message' => 'You do not have restaurant',
        //     ], 422);
        // }

        // Jika Failed
        if(!$data->save()) {
            throw new NotFoundHttpException();
        }

        // Jika Success
        return response()->json([
            'status' => 'ok',
            'ok' => true,
            'data' => $data
        ], 201);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $q = $request->get('q');
        $type = $request->get('type') ?? 'or';
        $strict = $request->get('strict') ?? 'true';
        $condition = [];

        if ($strict === 'true' || $strict === '1' || $strict) {
            foreach ($q as $key => $value) {
                array_push($condition, [$key, '=', $q[$key], $type]);
            }
        } 
        if ($strict === 'false' || $strict === '0' || !$strict) {
            foreach ($q as $key => $value) {
                array_push($condition, [$key, 'LIKE', '%' . $q[$key] . '%', $type]);
            }
        }
        $results = tbl_partner_review::
        where(
            $condition
        )
        ->where('deleted', '=', 0)
        ->paginate(10)->toArray();
        if(!$results) {
            throw new NotFoundHttpException();
        }

        // Return Response
        return response()->json([
            'status' => 'ok',
            'success'=>true,
            'page' => $results['current_page'] ?? '',
            'pageSize' => $results['per_page'] ?? '',
            'last' => $results['last_page'] ?? '',
            'total' => $results['total'] ?? '',
            'data' => $results['data'] ?? ''
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(tbl_partner_review $data, ReviewRequest $request)
    {
        // id user yg sedang login
        $auth_id = Auth::guard()->user()['id'];
        
        // check validasi input
        $data = new tbl_partner_review($request->all());
        $request['updated_by'] = $auth_id;
        $params = $request->route('id');
        
        $results = $data::
        where('deleted', '=', 0)
        ->where('created_by', '=', $auth_id)
        ->findOrFail($params);
        $results->touch();
        $results->update($request->except(['restaurant_id']));

        if (!$results) {
            throw new NotFoundHttpException();
        }

        return response()->json([
            'status' => 'ok',
            'ok' => true,
            'data' => $results
        ], 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
