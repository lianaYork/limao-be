<?php

namespace App\Http\Controllers\Partner;

use App\Http\Requests\Partner\PersonRequest;
use App\Model\Partner\tbl_partner_person;
use Auth;
use DB;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PersonController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', []);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params = $request->route('id');
        $auth_id = Auth::guard()->user()['id'];
        $results = tbl_partner_person::where('user_id', $auth_id)
            ->first();
        if (!$results) {
            throw new NotFoundHttpException();
        }
        return response()->json([
            'status' => 'ok',
            'ok' => true,
            'data' => $results,
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(PersonRequest $request)
    {
        // id user yg sedang login
        $auth_id = Auth::guard()->user()['id'];

        // check validasi input
        $user = new tbl_partner_person($request->all());

        // custom column yg tidak diinput
        $user['created_by'] = $auth_id;
        $user['user_id'] = $auth_id;

        $count = DB::table('tbl_partner_person')
            ->where('created_by', '=', $auth_id)
            ->where('deleted', '=', 0)
            ->count();

        // Check jika sudah ada data personal
        if ($count >= 1) {
            return response()->json([
                'status' => 'failed',
                'ok' => false,
                'count' => $count,
                'message' => 'You already have personal data',
            ], 422);
        }
        // Jika Failed
        if (!$user->save()) {
            throw new NotFoundHttpException();
        }

        // Jika Success
        return response()->json([
            'status' => 'ok',
            'ok' => true,
            'data' => tbl_partner_person::
                where([
                ['user_id', $auth_id],
            ])
                ->where('deleted', '=', 0)
                ->first(),
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        // Query Builder
        $person_name = $request->get('name') ?? '';

        $results = tbl_partner_person::
            where([
            ['name', 'like', '%' . $person_name . '%', 'or'],
        ])
            ->where('deleted', '=', 0)
        // ->limit(1)->get();
            ->paginate(2)->toArray();

        // Check results
        if (!$results) {
            throw new NotFoundHttpException();
        }

        // Return Response
        return response()->json([
            'status' => 'ok',
            'success' => true,
            'page' => $results['current_page'],
            'pageSize' => $results['per_page'],
            'last' => $results['last_page'],
            'total' => $results['total'],
            'data' => $results['data'],
            // 'data' => $results
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(tbl_partner_person $person, PersonRequest $request)
    {
        // id user yg sedang login
        $user_id = Auth::guard()->user()['id'];

        // check validasi input
        $user = new tbl_partner_person($request->all());
        $user['updated_by'] = $user_id;
        $params = $request->route('id');
        $count = DB::table('tbl_partner_person')
            ->where('user_id', '=', $user_id)
            ->where('deleted', '=', 0)
            ->count();
        if ($count < 1) {
            return response()->json([
                'status' => 'failed',
                'ok' => false,
                'count' => $count,
                'message' => 'Personal Data Not Found',
            ], 422);
        }
        $results = tbl_partner_person::where('user_id', '=', $user_id)
            ->where('deleted', '=', 0)
            ->first();
        $results->touch();
        $results->update($request->except(['user_id']));
        if (!$results) {
            throw new NotFoundHttpException();
        }
        return response()->json([
            'status' => 'ok',
            'ok' => true,
            'data' => $results,
        ], 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * approve the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
