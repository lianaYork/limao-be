<?php

namespace App\Http\Requests\Partner;

use Config;
use Dingo\Api\Http\FormRequest;

class OutletRequest extends FormRequest
{
    public function rules()
    {
        switch($this->method())
        {
            case 'POST':
                {
                    return Config::get('partner.outlet.create.validation_rules');
                }
                break;
            case 'PUT':
                {
                    return Config::get('partner.outlet.edit.validation_rules');
                }
                break;
            default:break;
            }
    }

    public function authorize()
    {
        return true;
    }
}
