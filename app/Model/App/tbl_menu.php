<?php

namespace App\Model\App;

use App\Model\App\tbl_menu;
use Eloquent;

class tbl_menu extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbl_menu';

    public function parent()
    {

        return $this->hasOne(tbl_menu::class, 'id', 'parent_id');

    }

    public function children()
    {

        return $this->hasMany(tbl_menu::class, 'parent_id', 'id');

    }

    public static function tree()
    {

        return static::with(implode('.', array_fill(0, 100, 'children')))->where('parent_id', '=', null)->orderBy('menu_id')->get();

    }

}
