<?php

namespace App\Model\Partner;

use Illuminate\Database\Eloquent\Model;

class tbl_partner_person extends Model
{
    protected $table = 'tbl_partner_person';

    protected $hidden = [
        'user_id',
        // 'email',
        'idType',
        'idNo',
        // 'address01',
        // 'taxNo',
        'deleted',
        'created_by',
        'updated_by',
    ];
    // protected $guarded = ['user_id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'email',
        'idType',
        'idNo',
        'latitude',
        'longitude',
        'address01',
        'address02',
        'taxNo',
        'phone',
        'whatsapp',
        'line',
        'telegram',
        'contact1',
        // 'deleted',
        // 'visit',
        // 'created_by',
        // 'updated_by',
        // 'created_at',
        // 'updated_at',
    ];
}
