<?php

namespace App\Model\Partner;

use Illuminate\Database\Eloquent\Model;

class tbl_partner_restaurant extends Model
{
    // use FullTextSearch;

    protected $table = 'tbl_partner_restaurant';

    // protected $searchable = ['name', 'email', 'address01', 'address02', 'phone'];

    protected $hidden = [
        'idType',
        'idNo',
        'taxNo',
        'companyNo',
        'whatsapp',
        'line',
        'telegram',
        'contact1',
        'email',
        'deleted',
        'approved',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
    ];
    // protected $guarded = ['person_id'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'person_id',
        'name',
        'latitude',
        'longitude',
        'city_id',
        'email',
        'idType',
        'idNo',
        'address01',
        'address02',
        'taxNo',
        'companyNo',
        'phone',
        'whatsapp',
        'line',
        'telegram',
        'contact1',
        // 'approved',
        // 'deleted',
        // 'visit',
        // 'created_by',
        // 'updated_by',
        // 'created_at',
        // 'updated_at',
    ];
}
