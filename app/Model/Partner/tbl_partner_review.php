<?php

namespace App\Model\Partner;

use Illuminate\Database\Eloquent\Model;

class tbl_partner_review extends Model
{
    protected $table = 'tbl_partner_review';
    
    protected $hidden = [
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'restaurant_id',
        'point',
        'caption',
        'description',
        // 'deleted',
        // 'visit',
        // 'created_by',
        // 'updated_by',
        // 'created_at',
        // 'updated_at',
    ];
}
