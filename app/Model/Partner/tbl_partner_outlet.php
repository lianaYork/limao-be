<?php

namespace App\Model\Partner;

use Illuminate\Database\Eloquent\Model;

class tbl_partner_outlet extends Model
{
    protected $table = 'tbl_partner_outlet';
    
    protected $hidden = [
        // 'restaurant_id',
        // 'city_id',
        // 'name',
        // 'latitude',
        // 'longitude',
        // 'address01',
        // 'address02',
        // 'phone',
        // 'whatsapp',
        // 'line',
        // 'telegram',
        // 'contact1',
        // 'deleted',
        // 'visit',
        'created_by',
        'updated_by',
        // 'created_at',
        // 'updated_at',
    ];
    // protected $guarded = ['restaurant_id'];    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'restaurant_id',
        'city_id',
        'name',
        'latitude',
        'longitude',
        'address01',
        'address02',
        'phone',
        'whatsapp',
        'line',
        'telegram',
        'contact1',
        // 'deleted',
        // 'visit',
        // 'created_by',
        // 'updated_by',
        // 'created_at',
        // 'updated_at',
    ];
}
