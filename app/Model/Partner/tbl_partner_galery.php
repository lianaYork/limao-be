<?php

namespace App\Model\Partner;

use Illuminate\Database\Eloquent\Model;

class tbl_partner_galery extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'restaurant_id',
        'photo_url',
        'name',
        'latitude',
        'longitude',
        'caption',
        'description',
        'review_id',
        'profile',
        'galery',
        'thumb',
        'deleted',
        'visit',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
    ];
}
