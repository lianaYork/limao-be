<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_districts extends Model
{
    //
    public function countries()
    {
        return $this->hasOne(tbl_cities::class);
    }

    protected $fillable = [
        'id', 'city_id', 'name', 'country_id', 'created_at',
    ];
}
